# ICE BECAME ICETOOL, check out on the [GITHUB WEBSITE](https://github.com/Art-Ev/ICEtool)

Basicly the philosophy hasn't changed, but it's now a real plugin in QGIS, and it's been simplified and optimized.
You should stop using ICE 1.0 and switch to ICEtool 1.0 and download it directly from QGIS

> **➜   [ICEtool website](https://github.com/Art-Ev/ICEtool)**



# OLD VERSION CHECK OUT 1ST PARAGRAPH ICE - version 1.0

Step by step models to estimate surface temperature of a neighborhood in Qgis 3.10 depending on surface materials and vegetation.

! ONLY TESTED WITH QGIS 3.10 ! (should work with higher versions)

What is it
---------------------
ICE are tools and a method to measure urban heat islands (UHI) and urban overheating, based on QGIS

ICE, is a collaboration between [Elioth](https://elioth.com/) and the teams of [Egis' Cities, Roads and Mobilities activity](https://www.egis.fr/activites/villes-0), to meet the needs of those who build the city 
 
ICE is :
* based on software used by all, fast, efficient: QGIS
* detailed enough and above all scientific enough: Urban Thermal Computing
with beautiful renderings and surface temperature data in order to compare the project and the existing or different project proposals: mapping
* Can be integrated in the current working methods and in the existing databases: GIS and OPENDATA

➜ This method allows to compare the existing with future projects as well as to quantify the impact of the developments and to target the critical points of the study area.

[**Documentation accessible here (FR)**](https://elioth.notion.site/ICE-Public-Documentation-bbdcbc00288d4e7d938142aa51c4a472)

[Find out more about the philosophy on Elioth website (FR)](https://elioth.com/ice-la-solution-pour-etudier-le-rafraichissement-de-vos-quartiers/)

![ICE by Elioth](https://elioth.com/wp-content/uploads/2021/09/2021-09-24-06_51_05-Window-1-1024x412.png)

6 steps inside QGIS
![ICE steps](https://elioth.com/wp-content/uploads/2021/09/ICE_etapes.png)

License
---------------------

ICE is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version. 
 
ICE is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License along with ICE; If not, see <http://www.gnu.org/licenses/>.
 
See License file for detailled informations

=======
Copyright elioth


See the License for the specific language governing permissions and limitations under the License.

# Contributors
---------------------
- [Stephanie Maalouf](https://www.linkedin.com/in/stephanie-maalouf/)
- [Marguerite Fournier](https://gitlab.com/m.fournier)
- [Louise Gontier](https://gitlab.com/l.gontier)
- [Guillaume Meunier](https://gitlab.com/alliages)
